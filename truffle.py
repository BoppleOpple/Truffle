import sys, os, struct, argparse
from PIL import Image

parser = argparse.ArgumentParser()

parser.add_argument("inputFile")
parser.add_argument("outputDirectory")

parser.add_argument("-c", "--combine", action = "store_true", help="saves all tiles to a single file")
parser.add_argument("--palette-only", action = "store_true", help="only saves a palette from the image, and ignores all tile creation")
parser.add_argument("--save-pngs", action = "store_true", help="save individual pngs for each tile (helpful for debugging)")
parser.add_argument("--export-yychr", action = "store_true", help="exports palette in RGB format (rounded to SNES first) for yy-chr")

parser.add_argument("-t", "--tilesize",
	type=int,
	default = 8,
	help="size of each tile, 8x8 or 16x16 for the SNES (default: 8)"
)
parser.add_argument("-b", "--bpp",
	type=int,
	default = 4,
	help="number of bits ber pixel (represents the number of colors in the palette (including transparency), 2bpp = 4 colors, 3bpp = 8, ... 8bpp = 256. default: 4)"
)

parser.add_argument("-p", "--palette", help="specifies a previously generated palette to reference (will add colors to outputted palette if they are missing)")

args = parser.parse_args()

#############
# Main Loop #
#############

def main():
	#########################
	# Variable Declarations #
	#########################

	tileMap = []
	mkdir(args.outputDirectory)
	
	if args.tilesize < 2:
		print(f"Error: Tiles must be at least 2x2. (Inputted: {args.tilesize})")
		exit()
	if args.bpp < 1 or args.bpp > 8:
		print(f"Error: Tiles must have between 1 and 8 bpp. (Inputted: {args.bpp})")
		exit()
	
	# Open selected image
	
	with Image.open(args.inputFile) as inputImage:
		if (not args.palette_only and not ( (inputImage.width % args.tilesize == 0) and (inputImage.height % args.tilesize == 0) )):
			print(f"Error: Image at {localToAbsolutePath(args.inputFile)} cannot be tiled evenly. (image of size {inputImage.width}x{inputImage.height} not divisible by selected tile size of {args.tilesize})")
			exit()
		else:

			# Palette
			
			palette = []
			if not (args.palette == None):
				with open(args.palette, 'rb') as importedPalette:
					palette = generatePalette(inputImage, args.bpp, list(importedPalette.read()))
			else:
				palette = generatePalette(inputImage, args.bpp)
			
			with open(os.path.join(args.outputDirectory, "Palette.pal"), 'wb') as f:
				if args.export_yychr:
					for i in range(0, len(palette), 2):
						snescolor, = struct.unpack(">h", bytes(palette[i:i+2]))
						r = int((255 / 31) * ((snescolor >>  0) & 0x1f))
						g = int((255 / 31) * ((snescolor >>  5) & 0x1f))
						b = int((255 / 31) * ((snescolor >> 10) & 0x1f))
						f.write(bytes([r, g, b]))
				else:
					f.write(bytes(palette))
					f.close()

			if not args.palette_only:
				# Tiles
				for x in range(0, inputImage.width, args.tilesize):
					for y in range(0, inputImage.height, args.tilesize):
						tileMap.append(Tile(inputImage.crop((x, y, x+args.tilesize, y+args.tilesize)), x, y))

				if args.save_pngs:
					saveTileImages(tileMap, args.outputDirectory)
				saveTileBinaries(tileMap, args.outputDirectory, palette, args.bpp, args.combine)



########################
# Function Definitions #
########################

def localToAbsolutePath(fp):
	absolutePathRaw = os.path.join(os.getcwd(), fp).split('/')[1:]
	absolutePathParsed = '/'
	for i in range(len(absolutePathRaw) - 1):
		if (not absolutePathRaw[i] in ["..", "."]) and (not absolutePathRaw[i+1] in [".."]):
			absolutePathParsed += absolutePathRaw[i] + '/'
	absolutePathParsed += absolutePathRaw[-1]
	return absolutePathParsed

def mkdir(dirPath):
	if not os.path.isdir(dirPath): 
		parent, directory = os.path.split(dirPath)
		if parent and not os.path.isdir(parent): mkdir(parent)
		if directory: os.mkdir(dirPath)

def saveTileImages(tm, dirPath):
	print(f"Saving tiles to {localToAbsolutePath(dirPath)}")
	for tile in tm:
		tile.image.save(os.path.join(dirPath, f"Tile-{tile.col}-{tile.row}.png"))

def saveTileBinaries(tm, dirPath, palette, bpp, join = False):
	print(f"Saving tile binaries to {localToAbsolutePath(dirPath)}")
	if join:
		bytesList = []
		for tile in tm:
			bytesList.extend(tile.toBytes(palette, bpp))

		with open(os.path.join(dirPath, f"Tileset.bin"), 'wb') as f:
			f.write(bytes(bytesList))
			f.close()
	else:
		for tile in tm:
			with open(os.path.join(dirPath, f"Tile-{tile.col}-{tile.row}.bin"), 'wb') as f:
				f.write(tile.toBytes(palette, bpp))
				f.close()

def generatePalette(img, bpp, initialPalette=[0, 0]):
	imageColors = list(map(lambda color: color[1], img.getcolors(maxcolors=2**bpp)))

	colorList = []
	groupedPalette = []
	for i in range(0, len(initialPalette), 2):
		colorList.append(tuple(initialPalette[i:i+2]))
	
	for color in imageColors:
		if color[3] != 0:
			colorList.append( colorToBytePair(color) )
	
	for color in colorList:
		if not color in groupedPalette:
			groupedPalette.append(color)

	palette = []
	for color in groupedPalette:
		palette.extend(color)

	if len(palette) > 2 * 2**bpp:
		print(f"Error: Image contains too many colors for the specified bpp. (Most colors at {bpp}bpp: {2**bpp}, attempted to store {int(len(palette)/2)})")
		exit()
	else:
		while len(palette) < 2 * 2**bpp:
			palette.extend([0, 0])
	return palette

def colorToBytePair(color):
	byteInt = ((color[2] >> 3) << 10) + ((color[1] >> 3) << 5) + (color[0] >> 3) # 0bbbbbgggggrrrrr, Shifting by 3 to reduce from 8 bits per color to 5 bits per color
	return ((byteInt & 0xff00) >> 8, byteInt & 0x00ff)

#####################
# Class Definitions #
#####################

class Tile:
	def __init__(self, PILImage, xOffset, yOffset):
		self.image = PILImage
		self.x = xOffset
		self.y = yOffset

		
		self.row = int(self.y/self.image.height)
		self.col = int(self.x/self.image.width)

	def index(self, palette):
		groupedPalette = []
		for byte in range(2, len(palette), 2):
			groupedPalette.append(palette[byte:byte+2])
		
		indexedPixels = []
		for y in range(self.image.height):
			indexedPixels.append([])
			for x in range(self.image.width):
				color = self.image.getpixel((x, y))
				if color[3] == 0:
					indexedPixels[y].append(0)
					continue

				pixelIndex = groupedPalette.index(list(colorToBytePair(color)))
				indexedPixels[y].append(pixelIndex + 1)
			
		return indexedPixels

	def toBytes(self, palette, bpp):
		indexMatrix = self.index(palette)
		byteList = []
		for bitplanePair in range(0, bpp, 2):
			i = 0
			for row in indexMatrix:
				i+=1
				byte = 0
				for bitOffset in range(2):
					bitMask = 1 << ( bitplanePair + bitOffset )
					for pixel in row:
						byte <<= 1
						byte += (pixel & bitMask) >> ( bitplanePair + bitOffset )
				byteList.extend([(byte & 0xff00) >> 8, byte & 0x00ff])
		return bytes(byteList)

	def __str__(self):
		return f"Tile Object at {self.x}, {self.y} (column {self.col}, row {self.row})"

##################
# Main Loop Init #
##################

if __name__ == "__main__":
	main()