# truffle 1.0
Truffle is a single-file image-to-snes converter intended to serve the same purpose as Bazz's pcx2snes but with 100% less pcx (because I hate its stupid format and so does GIMP)
## Use
Truffle is currently a CLI application (this may change later to maybe incorporate some features of yy-chr, if I get sick enough of using its weird ui through wine)
As a python file, all you need is python (I ran it with pypy3@7.3.9, which runs Python@3.7.13, as well as normal Python3@3.9.10, but it should function with most modern python versions) from the python site or apt or something. (I assume you know how to get python if you're using SNES assembly)

After downloading the repo, download one singular dependancy though the underused `pip install -r requirements.txt`, and read the following help page you should be able to use it, and if not just yell at me and maybe ill fix it ´\\(._.)/\`